//'use strict';
const express = require('express');
const app = new express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()) // to convert the request body to JSON
app.use(bodyParser.urlencoded({
    extended: true
}))

const fetch = require('node-fetch');
const external_api_url = "https://c3.avaamo.com";

var multiparty = require('multiparty');
var http = require('http');
const csv = require('csvtojson');
const csvWriter = require('csv-writer').createObjectCsvWriter; 
const XLSX = require('xlsx');

const parse      = require('csv-parse');
//const util       = require('util'); // not require can be taken out later
const fs         = require('fs');
//const path       = require('path'); // not require can be taken out later
const mysql      = require('mysql');
const async      = require('async');
const csvHeaders = require('csv-headers');
//const leftpad    = require('leftpad'); // not require can be taken out later

// database connection strings
const dbhost = "localhost"; // Db host
const dbuser = "root"; // DB user name
const dbpass = "root";//"root";  // DB password
const dbname = "testsuite"; // DB used for the application
const port = ""; // specify if any particular DB server port
const socketPath = "/Applications/MAMP/tmp/mysql/mysql.sock";  // only for mac os


var osvar = process.platform; // to identify which version of OS

/***********Begin of Mithun's code to generate sheets***************/

function generateEmptyTestSheets(){
  var request = require('request');
var excel = require('excel4node');
var express = require('express');
var router = express.Router();
var app = express();
var links, nodes, intents_used;
require('dotenv').config();

var sheet_styles = {
    "red":{
        font: {
            color: '#FF0800',
            bold:true,
            size: 12
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -'
    },
    "blue":{
        font: {
            color: '#295EA4',
            bold:true,
            size: 12
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -',
        alignment:{
            wrapText:true,
            horizontal: 'left'
        }
    },
    "black":{
        font: {
            color: '#0B0707',
            bold:true,
            size: 12
        },
        numberFormat: '$#,##0.00; ($#,##0.00); -',
        alignment:{
            wrapText:true,
            //horizontal: 'left',
            horizontal:'justify',
            vertical:'justify'
        }
    }
};

var sheet_options = {
    sheetFormat:{
        'defaultColWidth':50
    },
    sheetProtection:{
        'autoFilter': true,
        'deleteColumns': true,
        'deleteRows': true,
        'formatCells': true,
        'formatColumns': true,
        'formatRows': true,
        'insertColumns': true,
        'insertHyperlinks': true,
        'insertRows': true,
    }
}
}
async function getBotJSON() {
  return new Promise(function(resolve,reject){
      let options = {
          "method":"GET",
          "body":JSON.stringify({}),
          "url":"https://"+process.env.INSTANCE+".avaamo.com/dashboard/bots/"+process.env.BOTID+".json",
          "headers":{
              "content-type":"application/json",
              "access-token":"b145780f5ee24ef487f881e51a6051ad"//need to change
              //"Cookie":"_dashboard_session=b0I4M1hQT2t3UXBaaG1DUnlKdStzVjJOMEpzL2ZwWjZTTjg3VWpKUjVwUVh0UkhlRHJyakd4OW1Fa1FKUnE4VWM2TlgvMEZDLzhWWDdvTGR3QVFWekFhTis3K2RyNWdMbnpVZzIrZXB4ZFBKUG10MEk0NVVFSmdNcnZyU2FkOUpObXhHczloNTJMbGc1cU9zU0tNTmF6MTUrQjhiTkQzaW9sOGpKTzk0eW1KZURubTVMTitMb2pqQk5veTJyK1UyUGhpK2JhNnczUmRGdE94LzBhOGpkYWNyTHFrdks2ZWdHSE02Y1hOMEQzSWtFV3hSTzdTU3Jxb1BqSXVlZTRadzJuZ2lQOHVuM1R0dm02VUlNV3Y3ZkxLZlZZZUhRWlZmV3VvVzJCbXhscTd6emFlUUpNMGExdUZZbi9EVmFETTQrb2IrY1dlYXZPNllvVG5UWkxRVDZoZkFuVXBGcnpUYWJHOE0ya0ZGcDJHZ2xXK1pZeFhZVysraDhnRUdQTmwyOTFHdEw5b3Z2bE9DSVVrdFhRUmJCc2M3c2Z1VmlkRlkwdzZZSHd3d2hWU05sUlVxei8wa050bE83TkpJaWRmQW02RVdTOEFZMGRmS3BCUjdvTkp4MFNBSHpNM1VPZHB2OCs1c2JYSGhuK3V3Nk1QOTNWcDB3eDBaRTVRd2owTmhNV3JZMHRzN281Q04vV0t3MFVNWlF5TUhWaTNVeGRYeUdrYXZtUUs4UlduelNXMWY0WWpITlh2RW0vcFA1ZG54LS1ma1EvbW9qSmVjaXJ2REg2UHMwY0JBPT0%3D--8082e9aba4854ddf09cfd487e27e45505b813b80"
          }
      };
      request(options, async function(error,response,body){
      if(error){ console.log("Error"); reject();}
      resolve(JSON.parse(body));
      });
  });
}

async function initiateProcess(){
  let data = await getBotJSON();

  if(!data || data === undefined){ console.log("Error in API"); return false;}

  if(data && data.bot){
      links = data.bot.conversation_datum.conversation_json.links;
      nodes = data.bot.conversation_datum.conversation_json.nodes;
      intents_used = data.bot.conversation_datum.conversation_json.used_domain_intents;

  let top_level_intents = links.reduce((acc, item)=>{
      if(item.from == 1 && item.to !== "unhandled")acc.push(item);
      return acc;
  },[]);

  console.log("top level intents",top_level_intents);

  top_level_intents.map((item, index)=>{
      let workbook = new excel.Workbook(),
      intent_data = nodes.find((nodes)=>{ if(nodes.key == item.to){ return nodes;} }),
      previous_node_details = nodes.find((nodes)=>{ if(nodes.key == 1){ return nodes; }}),
      invocation_intent_sheet = workbook.addWorksheet(intent_data.intentData.name,sheet_options);

      invocation_intent_sheet.cell(1,1,1,10,true).string(FormatMessage(previous_node_details.messages)).style(workbook.createStyle(sheet_styles.blue));
      invocation_intent_sheet.cell(2,1).string(intent_data.intentData.name + "," + intent_data.key).style(workbook.createStyle(sheet_styles.black));
      iterate(item.to, workbook);
      workbook.write(intent_data.intentData.name + '.xlsx');
  });
  }
}

function FormatMessage(message_array) {
  let message_text = [];
  message_array.map((item, index)=>{
  switch(item.content_type){
      case "ssml":
      message_text.push(item.content.value.replace(/<[^>]*>?/gm, ''));
      break;
      case "dtmf":
      message_text.push(item.content);
      break;
      case "text":
      message_text.push(item.content.value);
      break;
      default:
      break;
  }
  });
  return message_text.join("\n");
}

function iterate(from_node, workbook){
  let gateway_node,
  childs = links.reduce((acc, links_items)=>{
      if(links_items.from == from_node)acc.push(links_items);
      return acc;
  },[]),
  from_node_details = nodes.find((item)=>{ if(item.key == from_node)return item; }),
  level_sheet = workbook.addWorksheet(from_node_details.intentData.name, sheet_options);

  childs.map((item, index)=>{
      let intent_data = nodes.find((nodes)=>{ if(nodes.key == item.to){ return nodes;} });
      if(intent_data){
          if(intent_data.goto_on_input == "" && (gateway_node == undefined || gateway_node == "")) gateway_node = intent_data.key;
          level_sheet.cell(1,1,1,childs.length,true).string(FormatMessage(from_node_details.messages)).style(workbook.createStyle(sheet_styles.blue));
          level_sheet.cell(2,index+1).string(intent_data.intentData.name + "," + intent_data.key).style(workbook.createStyle(sheet_styles.black));
      }
  });

  childs = links.reduce((acc, links_items)=>{
      if(links_items.from == gateway_node)acc.push(links_items);
      return acc;
  },[]);

  if(childs && childs.length > 0){ iterate(gateway_node, workbook); }
  else return;
}

/***********End of Mithun's code to generate sheets***************/

// util function to get DB connection object
function getConnectionObject(){
    return new Promise((resolve, reject) => {
      if(osvar=='darwin'){ // ofor osx
        var context = mysql.createConnection({
            host     : dbhost,
            user     : dbuser,
            password : dbpass,
            database : dbname,
          //  port : port,
            socketPath : socketPath, // only for mac
            multipleStatements: true
        });
      }else {
          var context = mysql.createConnection({
              host     : dbhost,
              user     : dbuser,
              password : dbpass,
              database : dbname,
              multipleStatements: true
            //  port : port,
          //  socketPath : socketPath
          });
      }
      context.connect((err) => {
            if (err) {
                console.error('error connecting: ' + err.stack);
                reject(err);
            } else {
              console.log("connected to database ");
                resolve(context);
            }
        });

    });
}


/* initiate Job queue for the test */
async function intiateJobQueue(req,res){
  var body = req.body;
  let rand_number = Math.floor(Math.random() * Math.floor(10000000));
  let file_name = "Regression_" + rand_number + ".csv";
  var req_body = {
      "validation_file_file_64": {
          "name": file_name,
          "size": body.data.length,
          "type": "text/csv",
          "data": body.data
      }
  };
  let options = {
      method: "PUT",
      body: JSON.stringify(req_body),
      headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Access-Token': 'b0f3392ca4d545a8a7b5172be3a46fc8'
      }
  };

  let result = await fetch(external_api_url + "/dashboard/bots/2320/self_validate.json", options)
      .then((response) => {
          return response.json();
      }).then((responseData) => {
          console.log("Regression response ", responseData);
          return responseData;
      }).catch((e) => {
          console.log("API Exception =============>>>>> ", e);
      });

  var ret_code = "200";
  if(result.validation_result.status =="IN_PROGRESS"){
    let validation_result = result.validation_result;
    let intents = JSON.stringify(body.intents);
    let connectionObject = await getConnectionObject() ;
    let sqlStatement =  mysql.format("INSERT INTO `preprocess_jobs`(`test_id`, `bot_id`, `test_status`, `bot_intents`, `input_file_file_name`) VALUES ('"+validation_result.id+"','"+validation_result.bot_id+"','"+validation_result.status+"','"+intents+"','"+validation_result.input_file_file_name+"')  ; ");
    connectionObject.query(sqlStatement, function (error, results, fields) {
      console.log(error);
      if(results.length >0){
          ret_code = "200";
      }else {
        ret_code = "400";
      }
    });
  }else {
      ret_code = "400";
  }
  res.write(JSON.stringify({
      "code": ret_code
  }));
  res.end();
  return;
}

/* fetch regression results locally */
async function getRegressionResults(){
  var connectionObject = await getConnectionObject();
  var sql =  mysql.format("SELECT * FROM `preprocess_jobs` WHERE `test_status` = 'IN_PROGRESS' ");
  connectionObject.query(sql, function (error, results, fields) {
    if(results.length >0){
        var urls = []; var testId = [];
        for (var i = 0; i < results.length; i++){
           let test_id = results[i].test_id;
           testId.push(test_id);
           let options = {
                method: "GET",
                headers: {
                     'Content-Type': 'text/csv',
                     'Access-Token': 'b0f3392ca4d545a8a7b5172be3a46fc8'
                }
            };
            urls.push(fetch(external_api_url + "/dashboard/validation_results/"+test_id+".csv",options).then(function(response) {
              return response.text();
            }) );
        }

        Promise.all(urls).then(csvStr => {
          var main_array = {};
          testId.forEach(function(item, index) { // filter the queued jobs
            if(csvStr[index] !== ''){ main_array[item] = csvStr[index]; }
          });
          var statements = "";
          for (var key in main_array) {
            statements += mysql.format("UPDATE `preprocess_jobs` SET `test_result`='" +main_array[key]+ "', `test_status`='COMPLETE' WHERE `test_id` = '" + key + "';");
          }

          if(statements!==""){
            connectionObject.query(statements, function(error, results, fields) {
                 console.log("results >>>>", results);
                 console.log("error >>>>", error);
            });
          }
      });
      return true ;
    }
  });
  return ;
}

/* get list of BOTS */
async function getBotList(req,res){
  var connectionObject = await getConnectionObject();
  let statement = mysql.format("SELECT * from `bot_manager`;");
  connectionObject.query(statement, function(error, results, fields) {
    if(results.length>0){
      res.write(JSON.stringify({
          "code": "200",
          "data": results
      }));
      res.end();
      return;
    }else {
      res.write(JSON.stringify({
          "code": "400",
          "data": results
      }));
      res.end();
      return;
    }
  });
}


/*get test results regression  */
async function getTestResult(req,res){
  var bot_id = req.headers.bot_id;
  if (!bot_id) {
      res.write(JSON.stringify({
          "code": "400",
          "msg": "BOT ID is required.",

      }));
      res.end();
      return;
  }
var connectionObject = await getConnectionObject();
let statement = mysql.format("SELECT * from `preprocess_jobs` WHERE `test_status` = 'COMPLETE' AND `bot_id` = '" + bot_id + "';");
connectionObject.query(statement, function(error, results, fields) {
    var ret_array = [];
    if (results.length > 0) {
        for (var i = 0; i < results.length; i++) {
            let test_id = results[i].test_id;
            let bot_id = results[i].bot_id;
            let bot_intents = JSON.parse(results[i].bot_intents);
            let test_result = results[i].test_result.split("\n");
            console.log(test_result);
            let query_result = [];
            for (var j = 1; j <= test_result.length; j++) {
                if (test_result[j]) {
                    let test_status = test_result[j].split(`",`)[1].split(",")[0];
                    let node_number = test_result[j].split(`",`)[1].split(",")[1];
                    let intent_name = bot_intents[node_number];
                    console.log(intent_name);
                    if (intent_name && test_status) {
                        query_result.push({
                            "intent_name": intent_name,
                            "test_status": test_status
                        });
                    }
                }
            }
            ret_array.push({
                "test_id": test_id,
                "bot_id": bot_id,
                "test_status": "COMPLETE",
                "query_result": query_result
            });
        }
        //  console.log(ret_array);
    }
    res.write(JSON.stringify({
        "code": "200",
        "data": ret_array
    }));
    res.end();
    return;
});
}

async function getTests(req, res, file) {
  try {
    const workbook = XLSX.readFile(file);
    let invalid = isInvalidWorkbook(workbook)
    if(invalid){
      res.write(JSON.stringify({
        "code": "400",
        'error': invalid
      }));
      res.end();
      return;
    }
    let tests = [];
    for (var key in workbook.Sheets) {
        if (workbook.Sheets.hasOwnProperty(key)) {
           console.log('\n processing sheet: '+ key + ' : started\n');
           let sheet = workbook.Sheets[key];
           tests = tests.concat(key === 'Invocation Intent' ? createTestsForInvocationIntent(sheet) : createTests(sheet));
           console.log('\n processing sheet: '+ key + ' : completed\n');
        }
    }
    console.log('\n Tests: ',tests);
    //let inputColumns = getInputColumns(MAX_INPUT);
    const _csvWriter = csvWriter({
      //header: ['path', ...inputColumns] 
      header: ['path', 'input']
    });
    let str = _csvWriter.csvStringifier.stringifyRecords(tests);
    let encodedTests = Buffer.from(str).toString('base64');
    res.write(JSON.stringify({
      "code": "200",
      data: encodedTests
    }));
    res.end();
    return;
  }
  catch (e) {
    res.write(JSON.stringify({
      "code": "500",
      "msg": "Internal error occured while processing your request.\n" + e.message,

    }));
    res.end();
    return;
  }
}
function createTestsForInvocationIntent(sheet){
  let tests = [];
  let range = XLSX.utils.decode_range(sheet['!ref']);
  let intent = sheet[XLSX.utils.encode_cell({ r: 0, c: 0 })];
  if(intent){
      let nodeNumber = intent.v.split(',')[1];
      for (let rowNum = 1; rowNum <= range.e.r; rowNum++) {
          let inputCell = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 0 })];
          if(inputCell){
              let test = {
                  path: nodeNumber + ',' + nodeNumber,
                  input: inputCell.v
              };
              console.log('\nadding new test\n', test);
              tests.push(test);
          }
      }
  }
  else{
      console.error('could not parse the invocation intent sheet');
  }
  return tests;
}
function isInvalidWorkbook(workbook) {
  let message = null;
  const MIN_INPUT_QUERIES = 1;
  for (var key in workbook.Sheets) {
    if (workbook.Sheets.hasOwnProperty(key)) {
      console.log('\n validating sheet: ' + key + '\n');
      let sheet = workbook.Sheets[key];
      let range = XLSX.utils.decode_range(sheet['!ref']);
      for(let col = 0; col <=  range.e.c; col++){
        let inputQueries = 0;
        let intentCell = key === 'Invocation Intent' ? sheet[XLSX.utils.encode_cell({ r: 0, c: col })] : sheet[XLSX.utils.encode_cell({ r: 1, c: col })] ;
        let intentName = intentCell.v.split(',')[0] ;
        for(let row = key === 'Invocation Intent' ? 1 : 2 ; row <= range.e.r; row++){
          let cell = sheet[XLSX.utils.encode_cell({ r: row, c: col })];
          if(cell && cell.v) ++inputQueries;
        }
        let error = 'Sheet[' + key + '] Intent[' + intentName + '] Error [Need atleast '+ MIN_INPUT_QUERIES +' queries] \n';
        if(inputQueries < MIN_INPUT_QUERIES) message = message ? message + error : error;
      }
    }
  }
  return message;
}
function createTests(sheet){
  let metadata = {
      parentIntentName: null,
      parentNodeNumber: null,
      transitions: []
  };
  let tests = [];
  let range = XLSX.utils.decode_range(sheet['!ref']);
  let parentIntent = sheet[XLSX.utils.encode_cell({ r: 0, c: 0 })];
  if(parentIntent){
      //prepare metadata
      let parent = parentIntent.v.split(',');
      metadata.parentIntentName = parent[0] ? parent[0].trim() : null;
      metadata.parentNodeNumber = parent[1] ? parent[1].trim(): null;
      for (let col = 0; col <= range.e.c; col++) {
          let childIntent = sheet[XLSX.utils.encode_cell({ r: 1, c: col })];
          if(childIntent){
              let values = childIntent.v.split(",");
              metadata.transitions.push({
                  intentName: values[0] ? values[0].trim() : null,
                  nodeNumber: values[1] ? values[1].trim() : null,
                  inputIndex: col,
              });
          }
      }
      console.log('\n node transitions info: ', metadata);

      //process input queries
      for (let row = 2; row <= range.e.r; row++) {
          for(let col=0; col <= range.e.c; col++){
              let inputCell = sheet[XLSX.utils.encode_cell({ r: row, c: col })];
              let transition = metadata.transitions.find(t=> t.inputIndex === col);
              if(inputCell && transition){
                  let test = {
                      path:  metadata.parentNodeNumber + ',' + transition.nodeNumber,
                      input: inputCell.v
                  };
                  console.log('\nadding new test\n', test);
                  tests.push(test);
              }
          }
      }
  }
  else{
      console.error('\n could not parse the sheet');
  }
  return tests;
}

/* Get JSON array out of CSV calls */
async function getJSONoutFromCSV(csvArray,results,connectionObject){
  var ret_json = [];
  csvArray.forEach(function(item, index) {
      ret_json.push(csv({
              //noheader:true,
              output: "json",
              headers: ['Flow', 'Result', 'Expected Node', 'Query', 'Matched Node'],
          })
          .fromString(item)
          .then((csvRow) => {
              return csvRow;
          }));
  });

  let jsonStr = Promise.all(ret_json).then(csvStr => {
    console.log(csvStr);
      let statements = "";
      for (var i = 0; i <= results.length; i++) {
          let test_id = results[i].test_id;
          statements += mysql.format("UPDATE `preprocess_jobs` SET `test_result`='" + JSON.stringify(csvStr[i]) + "', `test_status`='COMPLETE' WHERE `test_id` = '" + test_id + "';");
      }
      return csvStr;
  });
  return jsonStr;
}

module.exports = {
    intiateJobQueue: intiateJobQueue,
    getRegressionResults: getRegressionResults,
    getTestResult: getTestResult,
    getBotList : getBotList,
    getTests: getTests
};

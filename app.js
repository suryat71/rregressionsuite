//'use strict';
var express = require('express')
var bodyParser = require('body-parser')
const formidable = require('formidable')
const app = new express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use('/public', express.static('public'));
var multiparty = require('multiparty');
var http = require('http');
const https = require('https')

const fs = require('fs');
var url  = require('url');
var botapi = require('./botApi');
const appPort = "9031"; // node JS application port
global.base_url = "https://ai1.avaamo.com/vwdashboard_uat/uploadfile";
global.actual_base_url = "https://ai1.avaamo.com/vwdashboard_uat";
//const express = require('express');
const uuid = require('uuid/v4') ;
const session = require('express-session') ;
const fetch = require('node-fetch');
var heartbeats = require('heartbeats');

var heart = heartbeats.createHeart(30000);
heart.createEvent(1, function(count, last){
  console.log('...Every sinlge Beats forever',count);
  botapi.getRegressionResults();
});

// add & configure middleware
app.use(session({
  genid: (req) => {
//    console.log(req.sessionID)
    return uuid() // use UUIDs for session IDs
  },
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))

// create the homepage route at '/'
app.get('/index', (req, res) => {
  console.log("session "+req.session.loggedIN);
    if(req.session.loggedIN =="TRUE"){
       res.redirect(base_url);
    }else {
        console.log("loading login page");
      fs.readFile("login.html", function (error, pgResp) {
                   if (error) {
                       res.writeHead(404);
                       res.write('Contents you are looking are Not Found');
                   } else {
                       res.writeHead(200, { 'Content-Type': 'text/html' });
                       res.write(pgResp);
                   }
                   res.end();
       });
      return true ;
    }
})

app.get('/dashboard/uploadfile', (req, res) => {
  res.writeHead(200, {'Content-Type': 'text/html'});
  fs.createReadStream('home.html').pipe(res);
  return ;
})

app.get('/vwdashboard_uat/logout', (req, res) => {
    req.session.loggedIN = "FALSE";
    console.log("loggind out");
    res.redirect("https://ai1.avaamo.com/vwdashboard_uat/");
    return ;

    if(req.session.loggedIN =="TRUE"){
      // unset the sesion
      req.session.loggedIN = "FALSE";
      res.redirect(actual_base_url);
      return ;
    }else {
      // redirect to home
      res.redirect(actual_base_url);
    }
    return true ;
})

app.post('/dashboard/intiateRegressionQueue', (req, res) => {
    botapi.intiateJobQueue(req,res);
    return;
})
app.post('/dashboard/uploadRawTestsFile', (req, res) => {
  var form = new formidable.IncomingForm();

    form.parse(req);
    //Jai: should we store on particular path?
    // form.on('fileBegin', function (name, file){
    //     file.path = __dirname + '/uploads/' + file.name;
    // });

    form.on('file', function (name, file){
      if(name === 'rawTestsFile'){
        console.log('Uploaded ' + file.name);
        botapi.getTests(req,res,file.path);
      } 
      else {
        res.write(JSON.stringify({
          "code": "400",
          "msg": name + " is not an expected input"
        }));
        res.end();
        return;
      }
    });
  return;
})

app.post('/dashboard/getTestResult', (req, res) => {
    botapi.getTestResult(req,res);
    return;
})

app.get('/dashboard/getBotList', (req, res) => {
    botapi.getBotList(req,res);
    return;
})

// tell the server what port to listen on
app.listen(appPort, () => {
  console.log('Listening on localhost:'+appPort)
})
